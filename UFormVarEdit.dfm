object FormVarEdit: TFormVarEdit
  Left = 0
  Top = 0
  BorderStyle = bsSizeToolWin
  Caption = #1056#1077#1076#1072#1082#1090#1086#1088' '#1087#1077#1088#1077#1084#1077#1085#1085#1086#1081
  ClientHeight = 533
  ClientWidth = 590
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object pnlBottom: TPanel
    Left = 0
    Top = 492
    Width = 590
    Height = 41
    Align = alBottom
    TabOrder = 2
    DesignSize = (
      590
      41)
    object btnCancel: TButton
      Left = 500
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Cancel = True
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
    end
    object btnOk: TButton
      Left = 419
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #1059#1089#1090#1072#1085#1086#1074#1080#1090#1100
      Default = True
      TabOrder = 0
      OnClick = btnOkClick
    end
  end
  object pnTop: TPanel
    Left = 0
    Top = 0
    Width = 590
    Height = 85
    Align = alTop
    TabOrder = 0
    DesignSize = (
      590
      85)
    object Label1: TLabel
      Left = 4
      Top = 12
      Width = 23
      Height = 13
      Caption = #1048#1084#1103':'
    end
    object Label2: TLabel
      Left = 4
      Top = 63
      Width = 52
      Height = 13
      Caption = #1047#1085#1072#1095#1077#1085#1080#1077':'
    end
    object edtName: TEdit
      Left = 4
      Top = 31
      Width = 578
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      TabOrder = 0
      Text = 'EDTNAME'
      OnChange = edtNameChange
    end
    object btnInsertFile: TButton
      Left = 402
      Top = 59
      Width = 90
      Height = 23
      Action = actInsertFile
      Anchors = [akTop, akRight]
      Images = DataModuleArt.ilImages
      TabOrder = 2
    end
    object btnFolder: TButton
      Left = 492
      Top = 59
      Width = 90
      Height = 23
      Action = actInsertFolder
      Anchors = [akTop, akRight]
      Images = DataModuleArt.ilImages
      TabOrder = 3
    end
    object chkAsList: TCheckBox
      Left = 247
      Top = 62
      Width = 59
      Height = 17
      Action = actAsList
      Anchors = [akTop, akRight]
      TabOrder = 4
    end
    object btnInsertVar: TButton
      Left = 312
      Top = 59
      Width = 90
      Height = 23
      Anchors = [akTop, akRight]
      Caption = #1055#1077#1088#1077#1084#1077#1085#1085#1072#1103
      ImageIndex = 8
      Images = DataModuleArt.ilImages
      TabOrder = 1
      OnClick = btnInsertVarClick
    end
  end
  object mmoValue: TMemo
    Left = 0
    Top = 85
    Width = 590
    Height = 407
    Align = alClient
    Lines.Strings = (
      'mmoValue')
    ScrollBars = ssBoth
    TabOrder = 1
  end
  object actlst: TActionList
    Images = DataModuleArt.ilImages
    Left = 468
    Top = 128
    object actAsList: TAction
      AutoCheck = True
      Caption = #1057#1087#1080#1089#1082#1086#1084
      Hint = #1057#1087#1080#1089#1082#1086#1084
      OnExecute = actAsListExecute
    end
    object actInsertFile: TAction
      Caption = #1060#1072#1081#1083
      ImageIndex = 7
      OnExecute = actInsertFileExecute
    end
    object actInsertFolder: TAction
      Caption = #1055#1072#1087#1082#1072
      ImageIndex = 6
      OnExecute = actInsertFolderExecute
    end
  end
  object dlgOpen: TOpenDialog
    Filter = #1042#1089#1077' '#1092#1072#1081#1083#1099' (*.*)|*.*'
    Title = #1042#1099#1073#1086#1088' '#1092#1072#1081#1083#1072
    Left = 510
    Top = 128
  end
  object pmVarMenu: TPopupMenu
    Left = 420
    Top = 128
  end
end
