object FRMVarList: TFRMVarList
  Left = 0
  Top = 0
  Width = 427
  Height = 378
  TabOrder = 0
  OnResize = FrameResize
  object tlb: TToolBar
    Left = 0
    Top = 0
    Width = 427
    Height = 36
    AutoSize = True
    ButtonHeight = 36
    ButtonWidth = 98
    Caption = 'tlb'
    Images = DataModuleArt.ilImages
    ShowCaptions = True
    TabOrder = 0
    object ToolButton1: TToolButton
      Left = 0
      Top = 0
      Action = actAdd
    end
    object ToolButton2: TToolButton
      Left = 98
      Top = 0
      Action = actEdit
    end
    object ToolButton3: TToolButton
      Left = 196
      Top = 0
      Action = actDelete
    end
  end
  object actlst: TActionList
    Images = DataModuleArt.ilImages
    Left = 232
    Top = 76
    object actEdit: TAction
      Caption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1090#1100'...'
      ImageIndex = 3
      OnExecute = actEditExecute
    end
    object actAdd: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100'...'
      ImageIndex = 0
      OnExecute = actAddExecute
    end
    object actDelete: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
      ImageIndex = 1
      OnExecute = actDeleteExecute
    end
  end
  object pmRowMenu: TPopupMenu
    Images = DataModuleArt.ilImages
    Left = 164
    Top = 76
    object N2: TMenuItem
      Action = actEdit
    end
    object N3: TMenuItem
      Action = actDelete
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object N1: TMenuItem
      Action = actAdd
    end
  end
end
