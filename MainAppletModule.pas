unit MainAppletModule;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Classes, Vcl.Graphics, CtlPanel;

type
  TAppletModule1AppletModule = class(TAppletModule)
    procedure AppletModuleActivate(Sender: TObject; Data: Integer);
  private
  { private declarations }
  protected
  { protected declarations }
  public
  { public declarations }
  end;

var
  AppletModule1AppletModule: TAppletModule1AppletModule;

implementation

uses UMainForm;

{$R *.DFM}

procedure TAppletModule1AppletModule.AppletModuleActivate(Sender: TObject;
  Data: Integer);
var
  mf: TMainForm;
begin
  mf := TMainForm.Create(Application);
  try
    mf.ShowModal;
  finally
    mf.Free();
  end;
end;

end.