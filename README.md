
#  EnvEdit v1.0.0 (16.01.2021)

© 2021 kreakozeablik (kreakozeablik@yandex.ru)

![EnvEdit](./envedit.png)

Редактор переменных окружения.

[Скачать](https://bitbucket.org/kreakozeablik/envedit/downloads/)
