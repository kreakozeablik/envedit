unit UFormVarEdit;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.ToolWin,
  Vcl.ComCtrls, System.Actions, Vcl.ActnList, Vcl.Buttons, Vcl.Menus, UDataModuleArt;

type
  TFormVarEdit = class(TForm)
    pnlBottom: TPanel;
    btnCancel: TButton;
    btnOk: TButton;
    pnTop: TPanel;
    edtName: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    mmoValue: TMemo;
    actlst: TActionList;
    actAsList: TAction;
    btnInsertFile: TButton;
    btnFolder: TButton;
    actInsertFile: TAction;
    actInsertFolder: TAction;
    dlgOpen: TOpenDialog;
    chkAsList: TCheckBox;
    btnInsertVar: TButton;
    pmVarMenu: TPopupMenu;
    procedure edtNameChange(Sender: TObject);
    procedure actAsListExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure actInsertFileExecute(Sender: TObject);
    procedure actInsertFolderExecute(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure btnInsertVarClick(Sender: TObject);
  private
    fvalidatelst: TStrings;
    oldname: string;
    function SetAsList(vname: string): Boolean;
    procedure InsertMenuItemClick(Sender: TObject);
    procedure InitMenu();
    procedure SelectLine();
  public
    function Edit(var vname, vvalue: string; validatelst: TStrings): Boolean;
  end;

var
  FormVarEdit: TFormVarEdit;

implementation

uses Vcl.FileCtrl, System.Types;

{$R *.dfm}

procedure TFormVarEdit.SelectLine();
var
  line: Integer;
  length: Integer;
begin
  line := mmoValue.Perform(EM_LINEINDEX, WPARAM(-1),0);
  length := mmoValue.Perform(EM_LINELENGTH, WPARAM(-1), 0);
  mmoValue.SelStart := line;
  mmoValue.SelLength := length;
end;

procedure TFormVarEdit.edtNameChange(Sender: TObject);
begin
  btnOk.Enabled := edtName.Text <> '';
end;

procedure TFormVarEdit.FormCreate(Sender: TObject);
begin
  mmoValue.Lines.Delimiter := ';';
  mmoValue.Lines.StrictDelimiter := True;
end;

procedure TFormVarEdit.InitMenu;
  procedure AddMenuItem(s: string);
  var
    mi: TMenuItem;
  begin
    mi := TMenuItem.Create(self);
    mi.Caption := s;
    mi.OnClick := InsertMenuItemClick;
    pmVarMenu.Items.Add(mi);
  end;
  procedure AddMenuSeparator();
  var
    mi: TMenuItem;
  begin
    mi := TMenuItem.Create(self);
    mi.Caption := '-';
    pmVarMenu.Items.Add(mi);
  end;
var
  i: integer;
begin
  for i := 1 to fvalidatelst.Count - 2 do
    AddMenuItem(fvalidatelst[i]);

  AddMenuSeparator();
  AddMenuItem('ALLUSERSPROFILE');
  AddMenuItem('APPDATA');
  AddMenuItem('COMPUTERNAME');
  AddMenuItem('HOMEDRIVE');
  AddMenuItem('HOMEPATH');
  AddMenuItem('HOMESHARE');
  AddMenuItem('LOGONSERVER');
  AddMenuItem('SYSTEMDRIVE');
  AddMenuItem('SYSTEMROOT');
  AddMenuItem('USERDOMAIN');
  AddMenuItem('USERPROFILE');
end;

procedure TFormVarEdit.InsertMenuItemClick(Sender: TObject);
var
  s: string;
begin
  s := (Sender as TMenuItem).Caption;
  s := StringReplace(s, '&', '', []);
  mmoValue.SelText := '%' + s + '%';
end;

function TFormVarEdit.SetAsList(vname: string): Boolean;
var
  s: string;
begin
  Result := False;

  s := UpperCase(vname);
  if((s = 'PATH') or (s = 'PATHEXT') or (s = 'LM_LICENSE_FILE')) then
    Result := True;
end;

procedure TFormVarEdit.actAsListExecute(Sender: TObject);
begin
  if(actAsList.Checked)then
    mmoValue.Lines.DelimitedText := mmoValue.Text
  else
    mmoValue.Text := mmoValue.Lines.DelimitedText;
end;

procedure TFormVarEdit.actInsertFileExecute(Sender: TObject);
begin
  if(mmoValue.SelLength = 0)then
    SelectLine;

  dlgOpen.FileName := mmoValue.SelText;
  if(dlgOpen.Execute())then
    mmoValue.SelText := dlgOpen.FileName;
end;

procedure TFormVarEdit.actInsertFolderExecute(Sender: TObject);
var
  dir: string;
begin
  if(mmoValue.SelLength = 0)then
    SelectLine;

  dir := mmoValue.SelText;
  if(SelectDirectory('����� �����','\',dir,[sdNewFolder, sdNewUI, sdShowEdit, sdShowShares]))then
  mmoValue.SelText := dir;
end;

procedure TFormVarEdit.btnInsertVarClick(Sender: TObject);
var
  Pt: TPoint;
begin
  Pt := ClientToScreen(Point(btnInsertVar.Left, btnInsertVar.Top + btnInsertVar.Height));
  pmVarMenu.Popup(Pt.X, Pt.Y);
end;

procedure TFormVarEdit.btnOkClick(Sender: TObject);
var
  s: string;
begin
  if((oldname <> edtName.Text) and (fvalidatelst.IndexOf(edtName.Text) > 0))then
  begin
    s := '���������� � ������ "' + edtName.Text +'" ��� ����������!';
    Application.MessageBox(@s[1], PChar(Application.Title), MB_OK + MB_ICONERROR);
    Exit;
  end;

  ModalResult := mrOk;
end;

function TFormVarEdit.Edit(var vname, vvalue: string; validatelst: TStrings): Boolean;
begin
  fvalidatelst := validatelst;
  oldname := vname;

  edtName.Text := vname;

  if(SetAsList(vname))then
  begin
    actAsList.Checked := True;
    mmoValue.Lines.DelimitedText := vvalue;
  end else
    mmoValue.Text := vvalue;

  InitMenu;

  Result := (ShowModal = mrOk);

  if(Result)then
  begin
    vname := edtName.Text;

    if(actAsList.Checked)then
      vvalue := mmoValue.Lines.DelimitedText
    else
      vvalue := mmoValue.Text;

    vvalue := StringReplace(vvalue, #10, '', [rfReplaceAll]);
    vvalue := StringReplace(vvalue, #13, '', [rfReplaceAll]);
  end;
end;

end.
