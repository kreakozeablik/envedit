unit UFRMVarList;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Grids, System.Win.Registry,
  Vcl.Menus, System.Actions, Vcl.ActnList, Vcl.ToolWin, Vcl.ComCtrls, System.IniFiles,
  Vcl.StdCtrls, UDataModuleArt;

type
  TStringGridEx = class(TStringGrid)
  private
    FOnRButtonUp: TMouseEvent;
    FClickPos: Integer;
    FMenu: TPopupMenu;
  protected
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
  published
    property Menu: TPopupMenu read FMenu write FMenu;
    property ClickPos: Integer read FClickPos;
    property OnRButtonUp: TMouseEvent read FOnRButtonUp write FOnRButtonUp;
  end;

  TFRMVarList = class(TFrame)
    actlst: TActionList;
    pmRowMenu: TPopupMenu;
    actEdit: TAction;
    actAdd: TAction;
    actDelete: TAction;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    tlb: TToolBar;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    N4: TMenuItem;
    procedure strngrdSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
    procedure actDeleteExecute(Sender: TObject);
    procedure actEditExecute(Sender: TObject);
    procedure strngrdDblClick(Sender: TObject);
    procedure actAddExecute(Sender: TObject);
    procedure FrameResize(Sender: TObject);
    procedure strngrdMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
  private
    HintCol : integer;
    HintRow : integer;

    strngrd: TStringGridEx;

    FIsSystem: boolean;
    FCanEdit: Boolean;
    FModified: Boolean;

    procedure Enumerate();
    procedure AddToGrid(name, value: string);

    procedure SetVarValue(oldname, name, value: string);
    procedure DeleteVar(name: string);

    procedure SendNotyfy;

    function InitReg(r: TRegistry): Boolean;

    procedure UpdateValueColumnWidth;
    procedure UpdateActionEnabls(Row: Integer);
    procedure strngrdKeyPress(Sender: TObject; var Key: Char);
  public
    property Modified: Boolean read FModified;

    procedure UpdateNameColumnWidth;

    procedure Load(f: TIniFile);
    procedure Save(l: TStringList);
    procedure Init(p: boolean);
  end;

implementation

{$R *.dfm}

uses UFormVarEdit, System.Types;

{ TFRMVarList }

var
  fvarlst: array [0..1] of TFRMVarList;

procedure PrepareVarList(l: TStringList);
var
  i, j: Integer;
  s: string;
begin
  for j := 0 to 1 do
    if(fvarlst[0] <> nil)then
      for i := 0 to fvarlst[j].strngrd.Cols[0].Count-1 do
      begin
        s := fvarlst[j].strngrd.Cells[0, i];
        if((s <> '') and (l.IndexOf(s) < 0))then
          l.Add(s);
      end;

  l.Sort;
end;

procedure TFRMVarList.actAddExecute(Sender: TObject);
var
  vname: string;
  vvalue: string;
  dlg: TFormVarEdit;
begin
  dlg := TFormVarEdit.Create(Parent);
  try
    vname := '';
    vvalue := '';

    if(dlg.Edit(vname, vvalue, strngrd.Cols[0]))then
    begin
      SetVarValue('', vname, vvalue);
      AddToGrid(vname, vvalue);
      SendNotyfy;
      UpdateNameColumnWidth;
    end;
  finally
    dlg.Free;
  end;
end;

procedure TFRMVarList.actDeleteExecute(Sender: TObject);
var
  name: string;
  s: string;
  i: Integer;
begin
  if(strngrd.Row > 0) then
  begin
    name := strngrd.Cells[0, strngrd.Row];

    if(name <> '')then
    begin
      s := '������� ���������� "'+name+'"?';
      if Application.MessageBox(PChar(s), PChar(Application.Title), MB_YESNO + MB_ICONQUESTION + MB_DEFBUTTON2) = IDYES then
      begin
        DeleteVar(name);

        for i := strngrd.Row to strngrd.RowCount - 2 do
        begin
          strngrd.Cells[0, i] := strngrd.Cells[0, i + 1];
          strngrd.Cells[1, i] := strngrd.Cells[1, i + 1];
        end;

        strngrd.RowCount := strngrd.RowCount - 1;

        UpdateNameColumnWidth;
      end;
    end;
  end;
end;

procedure TFRMVarList.actEditExecute(Sender: TObject);
var
  voldname: string;
  vname: string;
  vvalue: string;
  dlg: TFormVarEdit;
  ind: Integer;
  i: Integer;
  varlst: TStringList;
begin
  dlg := TFormVarEdit.Create(self);
  try
    ind := strngrd.Row;
    vname := strngrd.Cells[0, ind];
    voldname := vname;
    vvalue := strngrd.Cells[1, ind];

    varlst := TStringList.Create;
    PrepareVarList(varlst);

    if(dlg.Edit(vname, vvalue, varlst))then
    begin

      SetVarValue(voldname, vname, vvalue);


      if(voldname <> vname)then
      begin
        if(voldname <> '')then
        begin
          for i := strngrd.Row to strngrd.RowCount - 2 do
          begin
            strngrd.Cells[0, i] := strngrd.Cells[0, i + 1];
            strngrd.Cells[1, i] := strngrd.Cells[1, i + 1];
          end;

          strngrd.RowCount := strngrd.RowCount - 1;
        end;

        AddToGrid(vname, vvalue)
      end else begin
        strngrd.Cells[1, ind] := vvalue;
      end;

      SendNotyfy;
      UpdateNameColumnWidth;
    end;

    varlst.Free;
  finally
    dlg.Free;
  end;
end;

procedure TFRMVarList.AddToGrid(name, value: string);
var
  ind: Integer;
  i: Integer;
  j: Integer;
  uname: string;
begin
  ind := strngrd.RowCount - 1;
  strngrd.RowCount := strngrd.RowCount + 1;

  uname := UpperCase(name);

  i := 1;
  while(i <  ind) do
  begin
    if(uname < UpperCase(strngrd.Cells[0, i]))then
      Break;

    Inc(i);
  end;

  for j := ind downto i + 1 do
  begin
    strngrd.Cells[0, j] := strngrd.Cells[0, j - 1];
    strngrd.Cells[1, j] := strngrd.Cells[1, j - 1];
  end;

  strngrd.Cells[0, i] := name;
  strngrd.Cells[1, i] := value;

  strngrd.Row := i;
end;

procedure TFRMVarList.DeleteVar(name: string);
var
  reg: TRegistry;
begin
  reg := TRegistry.Create;
  try
    InitReg(reg);

    reg.DeleteValue(name);

    reg.CloseKey;

    SendNotyfy;
  finally
    reg.Free;
  end;
end;

procedure TFRMVarList.Enumerate;
var
  reg: TRegistry;
  namelist: TStringList;
  name: string;
begin
  reg := TRegistry.Create;
  try
    namelist := TStringList.Create;
    try
      FCanEdit := False;
      if(InitReg(reg))then
      begin
        FCanEdit := True;

        reg.GetValueNames(namelist);

        for name in namelist do
          AddToGrid(name, reg.ReadString(name));

        reg.CloseKey;
      end else
        strngrd.Cells[1, 1] := '��������� ����� ��������������!';

       UpdateActionEnabls(strngrd.Row);
    finally
      namelist.Free;
    end;
  finally
    reg.Free;
  end;
end;

procedure TFRMVarList.FrameResize(Sender: TObject);
begin
  UpdateValueColumnWidth;
end;

procedure TFRMVarList.Init(p: Boolean);
begin
  if(p)then
    fvarlst[0] := self
  else
    fvarlst[1] := self;

  HintCol := 0;
  HintRow := 0;

  strngrd := TStringGridEx.Create(Self);
  with strngrd do
  begin
    Name := 'strngrd';
    Parent := self;
    Left := 0;
    Top := 29;
    Width := 427;
    Height := 349;
    Align := alClient;
    ColCount := 2;
    DefaultRowHeight := 19;
    FixedCols := 0;
    RowCount := 2;
    Options := [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRowSelect];
    TabOrder := 0;
    OnDblClick := strngrdDblClick;
    OnSelectCell := strngrdSelectCell;
    Cells[0, 0] := '����������';
    Cells[1, 0] := '��������';
    Menu := Self.pmRowMenu;
    OnMouseMove := strngrdMouseMove;
    ShowHint := True;
    OnKeyPress := strngrdKeyPress;
  end;

  FIsSystem := p;

  Enumerate;

  FModified := False;
end;

function TFRMVarList.InitReg(r: TRegistry): Boolean;
begin
  if(FIsSystem)then
  begin
    r.RootKey := HKEY_LOCAL_MACHINE;
    Result := r.OpenKey('System\CurrentControlSet\Control\Session Manager\Environment', false);
  end else begin
    r.RootKey := HKEY_CURRENT_USER;
    Result := r.OpenKey('Environment', false);
  end;
end;

procedure TFRMVarList.Load(f: TIniFile);
var
  vname: string;
  vvalue: string;
  section: string;
  reg: TRegistry;
  sl: TStringList;
begin
  reg := TRegistry.Create;
  try
    InitReg(reg);

    sl := TStringList.Create;
    try
      reg.GetValueNames(sl);

      strngrd.RowCount := 2;
      strngrd.Cells[0, 1] := '';
      strngrd.Cells[1, 1] := '';

      if(FIsSystem)then
        section := 'System'
      else
        section := 'User';

      f.ReadSection(section, sl);
      for vname in sl do
      begin
        vvalue := f.ReadString(section, vname, '');

        vvalue := StringReplace(vvalue, #10, '', [rfReplaceAll]);
        vvalue := StringReplace(vvalue, #13, '', [rfReplaceAll]);

        if(Pos('%', vvalue) > 0 )then
          reg.WriteExpandString(vname, vvalue)
        else
          reg.WriteString(vname, vvalue);

        AddToGrid(vname, vvalue);
      end;

    finally
      sl.Free;
    end;

    reg.CloseKey;
    SendNotyfy;
  finally
    reg.Free;
  end;
end;

procedure TFRMVarList.Save(l: TStringList);
var
  i: Integer;
begin
  if(FIsSystem)then
    l.Add('[System]')
  else
    l.Add('[User]');

  for i := 1 to strngrd.RowCount - 2 do
    l.Add(strngrd.Cells[0, i] +'="'+ strngrd.Cells[1, i] +'"');
end;

procedure TFRMVarList.SendNotyfy;
begin
  FModified := True;

  PostMessage(HWND_BROADCAST, WM_SETTINGCHANGE, 0, Integer(PChar('Environment')));
end;

procedure TFRMVarList.SetVarValue(oldname, name, value: string);
var
  reg: TRegistry;
begin
  reg := TRegistry.Create;
  try
    InitReg(reg);

    if((oldname <> '') and (oldname <> name))then
      reg.DeleteValue(oldname);

    if(Pos('%', value) > 0 )then
      reg.WriteExpandString(name, value)
    else
      reg.WriteString(name, value);

    reg.CloseKey;

    SendNotyfy;
  finally
    reg.Free;
  end;
end;

procedure TFRMVarList.strngrdMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
var
  r : integer;
  c : integer;
  sethint: Boolean;
  s: string;
begin
  strngrd.MouseToCell(X, Y, C, R);

  if ((HintRow <> r) or (HintCol <> c)) then
  begin
    HintRow := r;
    HintCol := c;
    Application.CancelHint;

    sethint := False;
    if((r > 0) and (c = 1))then
    begin
      s := strngrd.Cells[1, r];
      sethint := (strngrd.ColWidths[1] - 5) < strngrd.Canvas.TextWidth(s);
    end;

    if(sethint)then
      strngrd.Hint := strngrd.Cells[1, r]
    else
      strngrd.Hint := '';
  end;
end;

procedure TFRMVarList.strngrdDblClick(Sender: TObject);
begin
  if(strngrd.ClickPos > 0)then
    actEdit.Execute;
end;

procedure TFRMVarList.strngrdKeyPress(Sender: TObject; var Key: Char);
begin
  if(Key = #13)then
    actEdit.Execute;
end;

procedure TFRMVarList.strngrdSelectCell(Sender: TObject; ACol, ARow: Integer;
  var CanSelect: Boolean);
begin
  UpdateActionEnabls(ARow);
end;

procedure TFRMVarList.UpdateActionEnabls(Row: Integer);
begin
  actAdd.Enabled := FCanEdit;
  actEdit.Enabled := (Row > 0) and FCanEdit;
  actDelete.Enabled := (strngrd.Cells[0, Row] <> '') and FCanEdit;
end;

procedure TFRMVarList.UpdateNameColumnWidth;
var
  maxw: Integer;
  w: Integer;
  i: Integer;
begin
  maxw := 100;

  for i := 1 to strngrd.RowCount-1 do
  begin
    w := strngrd.Canvas.TextWidth(strngrd.Cells[0, i]) + 12;
    if(w > maxw)then
      maxw := w;
  end;

  strngrd.ColWidths[0] := maxw;

  UpdateValueColumnWidth;
end;

procedure TFRMVarList.UpdateValueColumnWidth;
var
  w: Integer;
begin
  w := strngrd.ClientWidth - strngrd.ColWidths[0] - 2;

  if(w < 100)then
    w := 100;

  strngrd.ColWidths[1] := w;

  HintRow := 0;
  HintCol := 0;
end;

{ TStringGridEx }

procedure TStringGridEx.MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  j: Integer;
  Pt: TPoint;
begin
  MouseToCell(X, Y, j, FClickPos);

  if (Button = mbRight) then
  begin
    if(FClickPos > 0)then
    begin
      Row := FClickPos;

      if(Assigned(FMenu))then
      begin
        Pt := ClientToScreen(Point(X, Y));
        FMenu.Popup(Pt.X, Pt.Y);
      end;
    end;

    if(Assigned(FOnRButtonUp))then
      FOnRButtonUp(Self, Button, Shift, X, Y);
  end;

  inherited;
end;

end.
