unit UDataModuleArt;

interface

uses
  System.SysUtils, System.Classes, Vcl.ImgList, Vcl.Controls, System.ImageList;

type
  TDataModuleArt = class(TDataModule)
    ilImages: TImageList;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DataModuleArt: TDataModuleArt;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

end.
