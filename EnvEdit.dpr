program EnvEdit;


uses
  Vcl.Forms,
  UMainForm in 'UMainForm.pas' {MainForm},
  UFRMVarList in 'UFRMVarList.pas' {FRMVarList: TFrame},
  UFormVarEdit in 'UFormVarEdit.pas' {FormVarEdit},
  UDataModuleArt in 'UDataModuleArt.pas' {DataModuleArt: TDataModule},
  UFormAbout in 'UFormAbout.pas' {FormAbout};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.Title := '���������� ���������';
  Application.CreateForm(TDataModuleArt, DataModuleArt);
  Application.CreateForm(TMainForm, MainForm);
  Application.Run;
end.
