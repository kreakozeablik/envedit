unit UMainForm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, System.Actions, Vcl.ActnList, Vcl.Menus,
  Vcl.ComCtrls, UFRMVarList, Vcl.ImgList, UDataModuleArt, UFormAbout;

type
  TMainForm = class(TForm)
    mmMain: TMainMenu;
    pgcMain: TPageControl;
    tsUser: TTabSheet;
    tsSystem: TTabSheet;
    actlst: TActionList;
    actExit: TAction;
    actSave: TAction;
    actLoad: TAction;
    actLoad1: TMenuItem;
    actLoad2: TMenuItem;
    actSave1: TMenuItem;
    actExit1: TMenuItem;
    FRMVarListUser: TFRMVarList;
    FRMVarListSystem: TFRMVarList;
    dlgOpen: TOpenDialog;
    dlgSave: TSaveDialog;
    N1: TMenuItem;
    actAbout: TAction;
    N2: TMenuItem;
    N3: TMenuItem;
    procedure actExitExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure pgcMainChange(Sender: TObject);
    procedure actSaveExecute(Sender: TObject);
    procedure actLoadExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure actAboutExecute(Sender: TObject);
  private
    initflag: Boolean;
  public
    { Public declarations }
  end;

var
  MainForm: TMainForm;

implementation

uses System.IniFiles;

{$R *.dfm}

procedure restart_PC;
var
  hToken: THandle;
  tkp: TTokenPrivileges;
  ReturnLength: Cardinal;
begin
  if OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES or
    TOKEN_QUERY, hToken) then
  begin
    LookupPrivilegeValue(nil, 'SeShutdownPrivilege', tkp.Privileges[0].Luid);
    tkp.PrivilegeCount := 1;
    tkp.Privileges[0].Attributes := SE_PRIVILEGE_ENABLED;
    if AdjustTokenPrivileges(hToken, False, tkp, 0, nil, ReturnLength) then
      ExitWindowsEx(EWX_REBOOT,0);
  end;
end;

procedure TMainForm.actAboutExecute(Sender: TObject);
begin
  with TFormAbout.Create(self) do
  begin
    ShowModal();
    Free();
  end;
end;

procedure TMainForm.actExitExecute(Sender: TObject);
begin
  Close;
end;

procedure TMainForm.actLoadExecute(Sender: TObject);
var
  f: TIniFile;
begin
  if(dlgOpen.Execute() and
    (Application.MessageBox('��������! ���������� ����� ����� ������������!',
    PChar(Application.Title), MB_OKCANCEL + MB_ICONWARNING + MB_TOPMOST) = IDOK))then
  begin
    f := TIniFile.Create(dlgOpen.FileName);
    try
      FRMVarListUser.Load(f);
      FRMVarListSystem.Load(f);
    finally
      f.Free;
    end;
  end;
end;

procedure TMainForm.actSaveExecute(Sender: TObject);
var
  s: string;
  sl: TStringList;
begin
  if(dlgSave.Execute())then
  begin
    if(FileExists(dlgSave.FileName))then
    begin
      s := '������������ ����: '+dlgSave.FileName+'?';
      if Application.MessageBox(PChar(s), PChar(Application.Title), MB_YESNO + MB_ICONQUESTION + MB_TOPMOST) = IDNO then
        Exit;
    end;

    sl := TStringList.Create;
    try
      FRMVarListUser.Save(sl);
      FRMVarListSystem.Save(sl);

      sl.SaveToFile(dlgSave.FileName);
    finally
      sl.Free;
    end;
  end;
end;

procedure TMainForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if(FRMVarListUser.Modified or FRMVarListSystem.Modified)then
  begin
     if (MessageBox(Application.Handle, '��� ���������� ��������� ���������� ������������� ��.'+#13+#10+'�������������?', 'EnvEdit', MB_ICONQUESTION or MB_YESNO) = idYes) then
        restart_PC();
  end;
end;

procedure TMainForm.FormCreate(Sender: TObject);
begin
  initflag := True;
  FRMVarListUser.Init(false);
  FRMVarListSystem.Init(True);
end;

procedure TMainForm.FormShow(Sender: TObject);
begin
  FRMVarListUser.UpdateNameColumnWidth;
end;

procedure TMainForm.pgcMainChange(Sender: TObject);
begin
  if(initflag)then
  begin
    FRMVarListSystem.UpdateNameColumnWidth;
    initflag := False;
  end;
end;

end.
